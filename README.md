# NetBox E164 plugin

A plugin for [NetBox](https://github.com/netbox-community/netbox) to manage E164 numbers.

`csb-netbox-plugin-e164` is using  [Netmiko](https://github.com/ktbyers/netmiko), [NAPALM](https://napalm.readthedocs.io/en/latest/) & [Django-RQ](https://github.com/rq/django-rq) to manage E164 numbers, update e164-pattern-mask on CUBE routers, and sync route-patters to Unified Communications Manager.

## Installation

If using the installation pattern from the NetBox Documentation, you will need to activate the
virtual environment before installing so that you install the package into the virtual environment.

```shell
cd /opt/netbox
source venv/bin/activate
```

The plugin is available as a Python package in pypi and can be installed with pip. Once the
installation is completed, then NetBox and the NetBox worker must be restarted.

```shell
pip install csb-netbox-plugin-e164
systemctl restart netbox netbox-rq
```

> The plugin is compatible with NetBox 2.11 and higher
 
To ensure NetBox Onboarding plugin is automatically re-installed during future upgrades, create a file named `local_requirements.txt` (if not already existing) in the NetBox root directory (alongside `requirements.txt`) and list the `csb-netbox-plugin-e164` package:

```no-highlight
# echo csb-netbox-plugin-e164 >> local_requirements.txt
```

Once installed, the plugin needs to be enabled in your `configuration.py`
```python
# In your configuration.py
PLUGINS = ["netbox_e164"]

# PLUGINS_CONFIG = {
#   "netbox_e164": {
#     ADD YOUR SETTINGS HERE
#   }
# }
```

Finally, make sure to run the migrations for this plugin

```bash
python3 manage.py migrate
```

The plugin behavior can be controlled with the following list of settings
- TBD

## Usage

You will need UCM and CUBE devices created with secrets. Also there will need to be an environment variable set to populate the secret for the rqworker process to login to the devices.

Use the E164 Task "add" button to create sync jobs for the UCM and CUBE devies. Logs will be listed on the E164 Tasks page.


### API

The plugin includes 6 API endpoints to manage the e164 numbers

```shell
GET        /api/plugins​/e164​/numbers​/       List e164 numbers
POST    ​   /api/plugins​/e164​/numbers​/       Add new e164 numbers
GET        /api/plugins​/e164​/numbers​/{id}/  Read e164 number
PUT     ​   /api/plugins​/e164​/numbers​/{id}/  Update e164 number
PATCH   ​   /api/plugins​/e164​/numbers​/{id}/  Partial update e164 numbers
DELETE     /api/plugins/e164/numbers/{id}/  Delete e164 number
```

## Contributing

Pull requests are welcomed and automatically built and tested against multiple version of Python and multiple version of NetBox through TravisCI.

The project is packaged with a light development environment based on `docker-compose` to help with the local development of the project and to run the tests within TravisCI.

The project is following Network to Code software development guideline and is leveraging:
- Black, Pylint, Bandit and pydocstyle for Python linting and formatting.
- Django unit test to ensure the plugin is working properly.


## Dev Notes
AJAX/JQuery based dependent dropdowns based on this [tutorial](https://simpleisbetterthancomplex.com/tutorial/2018/01/29/how-to-implement-dependent-or-chained-dropdown-list-with-django.html)

### CLI Helper Commands

The project is coming with a CLI helper based on [invoke](http://www.pyinvoke.org/) to help setup the development environment. The commands are listed below in 3 categories `dev environment`, `utility` and `testing`. 

Each command can be executed with `invoke <command>`. All commands support the arguments `--netbox-ver` and `--python-ver` if you want to manually define the version of Python and NetBox to use. Each command also has its own help `invoke <command> --help`

#### Local dev environment
```
  build            Build all docker images.
  debug            Start NetBox and its dependencies in debug mode.
  destroy          Destroy all containers and volumes.
  start            Start NetBox and its dependencies in detached mode.
  stop             Stop NetBox and its dependencies.
```

#### Utility 
```
  cli              Launch a bash shell inside the running NetBox container.
  create-user      Create a new user in django (default: admin), will prompt for password.
  makemigrations   Run Make Migration in Django.
  nbshell          Launch a nbshell session.
```
#### Testing 

```
  tests            Run all tests for this plugin.
  pylint           Run pylint code analysis.
  pydocstyle       Run pydocstyle to validate docstring formatting adheres to NTC defined standards.
  bandit           Run bandit to validate basic static code security analysis.
  black            Run black to check that Python files adhere to its style standards.
  unittest         Run Django unit tests for the plugin.
```

## Questions

For any questions or comments, please check the [FAQ](FAQ.md) first and feel free to email us at csbdev@cspire.com
