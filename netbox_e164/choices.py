from utilities.choices import ChoiceSet


class E164TaskTypes(ChoiceSet):

    UCM_PARTITION_SYNC = "ucm_partition_sync"
    UCM_TRUNK_SYNC = "ucm_trunk_sync"
    UCM_ROUTEPATTERN_SYNC = "ucm_routepattern_sync"
    CUBE_E164MAP_SYNC = "cube_e164map_sync"

    CHOICES = (
        (UCM_PARTITION_SYNC, "UCM Partition Sync"),
        (UCM_TRUNK_SYNC, "UCM Trunk Sync"),
        (UCM_ROUTEPATTERN_SYNC, "UCM Route-Pattern Sync"),
        (CUBE_E164MAP_SYNC, "CUBE e164-pattern-map Sync"),
    )


class E164TaskStatus(ChoiceSet):
    """Valid values for E164Task "status"."""

    STATUS_FAILED = "failed"
    STATUS_PENDING = "pending"
    STATUS_RUNNING = "running"
    STATUS_SUCCEEDED = "succeeded"

    CHOICES = (
        (STATUS_FAILED, "failed"),
        (STATUS_PENDING, "pending"),
        (STATUS_RUNNING, "running"),
        (STATUS_SUCCEEDED, "succeeded"),
    )


class E164TrunkTypes(ChoiceSet):
    """Valid values for E164Trunk Type"""

    SIP_TRUNK = "sip_trunk"
    ROUTE_LIST = "route_list"

    CHOICES = (
        (SIP_TRUNK, "SIP Trunk"),
        (ROUTE_LIST, "Route-list"),
    )
