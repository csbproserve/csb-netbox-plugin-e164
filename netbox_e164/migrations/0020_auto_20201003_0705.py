# Generated by Django 3.1 on 2020-10-03 07:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("netbox_e164", "0019_auto_20201003_0658"),
    ]

    operations = [
        migrations.AlterField(
            model_name="e164deletednumber", name="deleted_on_ucm", field=models.BooleanField(default=False),
        ),
    ]
