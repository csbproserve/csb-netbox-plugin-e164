# Generated by Django 3.1 on 2020-10-03 11:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("netbox_e164", "0021_auto_20201003_0710"),
    ]

    operations = [
        migrations.AddField(
            model_name="e164deletednumber", name="ucm_uuid", field=models.UUIDField(blank=True, null=True),
        ),
        migrations.AddField(model_name="e164number", name="ucm_uuid", field=models.UUIDField(blank=True, null=True),),
    ]
