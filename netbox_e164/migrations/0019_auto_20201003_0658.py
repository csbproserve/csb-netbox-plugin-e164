# Generated by Django 3.1 on 2020-10-03 06:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("netbox_e164", "0018_auto_20201003_0649"),
    ]

    operations = [
        migrations.AlterField(
            model_name="e164deletednumber", name="deleted_on_ucm", field=models.BooleanField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name="e164deletednumber",
            name="deleted_on_ucm_date",
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
