"""Forms for e164 number management.

(c) 2020 C Spire Business
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from django import forms
from django.core.validators import RegexValidator
from django_rq import get_queue
from django.conf import settings

from utilities.forms import BootstrapMixin, CSVModelForm
from extras.forms import CustomFieldModelForm
from tenancy.forms import TenancyForm
from dcim.models import Site, Device
from tenancy.models import Tenant
from extras.forms import CustomFieldModelCSVForm

PLUGIN_SETTINGS = settings.PLUGINS_CONFIG["netbox_e164"]

from .models import E164Number, E164Partition, E164Trunk, E164Task, E164TaskLogEntry

BLANK_CHOICE = (("", "---------"),)


class E164NumberForm(BootstrapMixin, TenancyForm, CustomFieldModelForm):
    """Form for creating or editing an E164Number instance"""

    number = forms.CharField(
        required=True,
        label="E164 Number",
        help_text="Enter number in E.164 format (including +). UCM wildcards are permissable.",
        validators=[
            RegexValidator(
                regex=r"^\+?[1-9][\d\[\]\-X]{1,50}$",
                message="Enter a valid E164 number including the + sign. Follow UCM Wildcard conventions.",
            )
        ],
    )

    site = forms.ModelChoiceField(required=True, queryset=Site.objects.all())

    # tenant = forms.ModelChoiceField(required=True, queryset=Tenant.objects.all())

    description = forms.CharField(required=False, label="Description")

    ucm_role = PLUGIN_SETTINGS["ucm_role"]
    ucm = forms.ModelChoiceField(required=True, queryset=Device.objects.filter(device_role__slug=ucm_role))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = kwargs.get("instance")

        # prime drop downs for existing record
        if instance and instance.ucm is not None:
            initial = kwargs.get("initial", {}).copy()
            initial["ucm"] = instance.ucm
            self.fields["partition"].queryset = E164Partition.objects.filter(ucm=initial["ucm"])
            self.fields["trunk"].queryset = E164Trunk.objects.filter(ucm=initial["ucm"])
        else:
            self.fields["partition"].queryset = E164Partition.objects.none()
            self.fields["trunk"].queryset = E164Trunk.objects.none()

        # prime drop downs for new record on post
        if "ucm" in self.data:
            try:
                ucm_id = int(self.data.get("ucm"))
                self.fields["partition"].queryset = E164Partition.objects.filter(ucm=ucm_id)
                self.fields["trunk"].queryset = E164Trunk.objects.filter(ucm=ucm_id)
            except:
                pass

    class Meta:
        model = E164Number
        fields = ["number", "site", "tenant_group", "tenant", "description", "ucm", "partition", "trunk"]


class E164NumberListFilterForm(BootstrapMixin, forms.ModelForm):
    """Form for filtering E164NumberList instances"""

    site = forms.ModelChoiceField(queryset=Site.objects.all(), required=False, to_field_name="slug")

    tenant = forms.ModelChoiceField(queryset=Tenant.objects.all(), required=False, to_field_name="slug")

    q = forms.CharField(required=False, label="Search")

    class Meta:  # noqa: D106 "Missing docstring in public nested class"
        model = E164Number
        fields = [
            "q",
            "site",
            "tenant",
        ]


class E164NumberCSVForm(CSVModelForm):
    """Form for entering CSV to bulk-import E164Number entries."""

    tenant = forms.ModelChoiceField(
        queryset=Tenant.objects.all(),
        required=True,
        to_field_name="slug",
        help_text="Slug of parent tenant",
        error_messages={"invalid_choice": "Tenant not found",},
    )
    site = forms.ModelChoiceField(
        queryset=Site.objects.all(),
        required=True,
        to_field_name="slug",
        help_text="Slug of parent site",
        error_messages={"invalid_choice": "Site not found",},
    )
    number = forms.CharField(required=True, help_text="E164 Number including + sign")
    description = forms.CharField(required=False, help_text="short description of number usage")

    class Meta:
        model = E164Number
        fields = E164Number.csv_headers

    def save(self, commit=True, **kwargs):
        """Save the model"""

        model = super().save(commit=commit, **kwargs)
        return model


class E164TaskListFilterForm(BootstrapMixin, forms.ModelForm):
    """Form for filtering E164TaskList instances"""

    q = forms.CharField(required=False, label="Search")

    class Meta:
        model = E164Task
        fields = [
            "q",
            "device",
            "type",
            "status",
        ]


class E164TaskLogEntryFilterForm(BootstrapMixin, forms.ModelForm):
    """Form for filtering E164TaskLogEntry instances"""

    q = forms.CharField(required=False, label="Search")

    class Meta:
        model = E164TaskLogEntry
        fields = [
            "q",
            "task",
            "source",
            "message",
        ]
