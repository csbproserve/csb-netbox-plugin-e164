from os.path import abspath, dirname
from os import getenv
import logging
import inspect
from datetime import datetime

from urllib.parse import urljoin
from urllib.request import pathname2url
import zeep
import requests
import textfsm

from napalm import get_network_driver

from django.conf import settings
from django_rq import job

from dcim.models import Device, DeviceRole
from secrets.models import Secret

from .models import E164Task, E164TaskLogEntry, E164Partition, E164Trunk, E164Number, E164DeletedNumber
from .choices import E164TaskTypes, E164TaskStatus, E164TrunkTypes
from .utils.secrets import E164Secrets

PLUGIN_SETTINGS = settings.PLUGINS_CONFIG["netbox_e164"]


def get_ucm_service(ucm_ip, username, password):

    WSDL = urljoin("file:", pathname2url(dirname(__file__) + ("/cisco_axl/AXLAPI.wsdl")))

    session = requests.Session()
    session.auth = requests.auth.HTTPBasicAuth(username, password)
    session.verify = False
    transport_with_basic_auth = zeep.transports.Transport(session=session)

    CLIENT = zeep.Client(WSDL, transport=transport_with_basic_auth)
    service = CLIENT.create_service(
        "{http://www.cisco.com/AXLAPIService/}AXLAPIBinding", "https://" + ucm_ip + ":8443/axl/"
    )

    return service


def sync_e164(dev_id):
    """Entrypoint for RQ. Get secrets master key. Determine device type and handoff to device specific sync function."""

    secret_user = getenv("RQ_WORKER_SECRET_USER")
    secret_key = getenv("RQ_WORKER_SECRET_KEY")

    master_key = E164Secrets.getMasterKey(secret_user, E164Secrets.getSessionKey(secret_user, secret_key))

    if master_key is not None:
        ucm_role = DeviceRole.objects.get(slug=PLUGIN_SETTINGS["ucm_role"])
        cube_role = DeviceRole.objects.get(slug=PLUGIN_SETTINGS["cube_role"])

        sync_device = Device.objects.get(id=dev_id)

        if sync_device.device_role == ucm_role:
            sync_e164_ucm(sync_device, master_key)
        elif sync_device.device_role == cube_role:
            sync_e164_cube(sync_device, master_key)


def sync_e164_ucm_partitions(e164_logger, ucm_service, sync_device):
    """Send SOAP request. Check for partitions and create if needed. Delete partitions removed from UCM.
        NOTE: model is set to protect, so partition will fail to delete if E164Numbers are tied to it
    """

    source = inspect.currentframe().f_code.co_name
    partition_list_response = ucm_service.listRoutePartition(
        searchCriteria={"name": "%",}, returnedTags={"uuid": True, "name": True,}
    )

    if partition_list_response["return"]:
        ucm_partitions = partition_list_response["return"]["routePartition"]
    else:
        ucm_partitions = []

    e164_logger.sync_partition_info(source, f"Collected {ucm_partitions.__len__()} partitions from {sync_device.name}")

    nb_partitions = E164Partition.objects.filter(ucm=sync_device)
    e164_logger.sync_partition_info(source, f"Comparing to {nb_partitions.count()} partitions in Netbox.")

    for partition in ucm_partitions:
        try:
            nb_partition = E164Partition.objects.get(ucm_uuid=partition["uuid"])
            if nb_partition.name != partition["name"]:
                e164_logger.sync_partition_info(
                    source, f"Partition {nb_partition.name} name changed to {partition['name']}in UCM. Updating Netbox."
                )
                nb_partition.name = partition["name"]
                nb_partition.save()
        except E164Partition.DoesNotExist:
            E164Partition.objects.create(name=partition["name"], ucm_uuid=partition["uuid"], ucm=sync_device)
            e164_logger.sync_partition_info(source, f"Created partition {partition['name']} in Netbox.")

    for nb_partition in nb_partitions:
        try:
            next(part for part in ucm_partitions if part["uuid"].lower() == "{" + nb_partition.ucm_uuid.__str__() + "}")
        except StopIteration:
            try:
                nb_partition.delete()
                e164_logger.sync_partition_info(source, f"Deleted partition {nb_partition.name} in Netbox.")
            except:
                e164_logger.sync_partition_info(
                    source, f"Could not delete stale partition {nb_partition.name} in Netbox."
                )


def sync_e164_ucm_trunks(e164_logger, ucm_service, sync_device):
    """Send SOAP request. Check for trunks/route-lists and create if needed. Delete trunks/route-lists removed from UCM.
        NOTE: model is set to protect, so trunk will fail to delete if E164Numbers are tied to it
    """

    source = inspect.currentframe().f_code.co_name

    trunk_list_response = ucm_service.listSipTrunk(
        searchCriteria={"name": "%",}, returnedTags={"uuid": True, "name": True,}
    )

    if trunk_list_response["return"]:
        ucm_trunks = trunk_list_response["return"]["sipTrunk"]
    else:
        ucm_trunks = []

    e164_logger.sync_trunk_info(source, f"Collected {ucm_trunks.__len__()} trunks from {sync_device.name}")

    nb_trunks = E164Trunk.objects.filter(type=E164TrunkTypes.SIP_TRUNK, ucm=sync_device)
    e164_logger.sync_trunk_info(source, f"Comparing to {nb_trunks.count()} trunks in Netbox.")

    for trunk in ucm_trunks:
        try:
            nb_trunk = E164Trunk.objects.get(ucm_uuid=trunk["uuid"], ucm=sync_device)
            if nb_trunk.name != trunk["name"]:
                e164_logger.sync_trunk_info(
                    source, f"Trunk {nb_trunk.name} name changed to {trunk['name']}in UCM. Updating Netbox."
                )
                nb_trunk.name = trunk["name"]
                nb_trunk.save()
        except E164Trunk.DoesNotExist:
            E164Trunk.objects.create(
                name=trunk["name"], type=E164TrunkTypes.SIP_TRUNK, ucm_uuid=trunk["uuid"], ucm=sync_device
            )
            e164_logger.sync_trunk_info(source, f"Created trunk {trunk['name']} in Netbox.")

    for nb_trunk in nb_trunks:
        try:
            next(part for part in ucm_trunks if part["uuid"].lower() == "{" + nb_trunk.ucm_uuid.__str__() + "}")
        except StopIteration:
            try:
                nb_trunk.delete()
                e164_logger.sync_trunk_info(source, f"Deleted trunk {nb_trunk.name} in Netbox.")
            except:
                e164_logger.sync_trunk_info(source, f"Could not delete stale trunk {nb_trunk.name} in Netbox.")

    routelist_list_response = ucm_service.listRouteList(
        searchCriteria={"name": "%",}, returnedTags={"uuid": True, "name": True,}
    )

    if routelist_list_response["return"]:
        ucm_routelists = routelist_list_response["return"]["routeList"]
    else:
        ucm_routelists = []

    e164_logger.sync_trunk_info(source, f"Collected {ucm_routelists.__len__()} route-lists from {sync_device.name}")

    nb_routelists = E164Trunk.objects.filter(type=E164TrunkTypes.ROUTE_LIST, ucm=sync_device)
    e164_logger.sync_trunk_info(source, f"Comparing to {nb_routelists.count()} route-lists in Netbox.")

    for routelist in ucm_routelists:
        try:
            nb_routelist = E164Trunk.objects.get(ucm_uuid=routelist["uuid"], ucm=sync_device)
            if nb_routelist.name != routelist["name"]:
                e164_logger.sync_trunk_info(
                    source,
                    f"Route-list {nb_routelist.name} name changed to {routelist['name']}in UCM. Updating Netbox.",
                )
                nb_routelist.name = routelist["name"]
                nb_routelist.save()
        except E164Trunk.DoesNotExist:
            E164Trunk.objects.create(
                name=routelist["name"], type=E164TrunkTypes.ROUTE_LIST, ucm_uuid=routelist["uuid"], ucm=sync_device
            )
            e164_logger.sync_trunk_info(source, f"Created route-list {routelist['name']} in Netbox.")

    for nb_routelist in nb_routelists:
        try:
            next(
                routelist
                for routelist in ucm_routelists
                if routelist["uuid"].lower() == "{" + nb_routelist.ucm_uuid.__str__() + "}"
            )
        except StopIteration:
            try:
                nb_routelist.delete()
                e164_logger.sync_trunk_info(source, f"Deleted route-list {nb_routelist.name} in Netbox.")
            except:
                e164_logger.sync_trunk_info(source, f"Could not delete stale route-list {nb_routelist.name} in Netbox.")


def get_ucm_pattern(number):
    """check for +, add \\"""

    if number.startswith("+"):
        ucm_pattern = f"\{number}"
    else:
        ucm_pattern = number

    return ucm_pattern


def get_ucm_destination(e164number):
    """Build proper destination dict for UCM"""
    ucm_destination = None
    if e164number.trunk.type == E164TrunkTypes.ROUTE_LIST:
        ucm_destination = {"routeListName": e164number.trunk.name}
    elif e164number.trunk.type == E164TrunkTypes.SIP_TRUNK:
        ucm_destination = {"gatewayName": e164number.trunk.name}
    return ucm_destination


def get_ucm_description(description):
    routepattern_desc_prefix = PLUGIN_SETTINGS["ucm_routepattern_desc_prefix"]
    return routepattern_desc_prefix + description


def sync_ucm_e164_verify_number(e164_logger, ucm_service, e164number):
    """Send Get and verify parameters match, return true or false, set sync'd and verified date if true on E164Number
        :e164_logger as E164TaskLogger
        :ucm_service as zeep.Client.create_service()
        :e164_number as E164Number instance
    """

    source = inspect.currentframe().f_code.co_name

    try:
        routepattern_get_response = ucm_service.getRoutePattern(uuid=e164number.ucm_uuid)

        match = True

        ucm_pattern = get_ucm_pattern(e164number.number)
        if routepattern_get_response["return"]["routePattern"]["pattern"] != ucm_pattern:
            match = False
        if (
            routepattern_get_response["return"]["routePattern"]["routePartitionName"]["_value_1"]
            != e164number.partition.name
        ):
            match = False
        if e164number.trunk.type == E164TrunkTypes.SIP_TRUNK:
            if (
                routepattern_get_response["return"]["routePattern"]["destination"]["gatewayName"]["uuid"].lower()
                != "{" + e164number.trunk.ucm_uuid.__str__() + "}"
            ):
                match = False
        elif e164number.trunk.type == E164TrunkTypes.ROUTE_LIST:
            if (
                routepattern_get_response["return"]["routePattern"]["destination"]["routeListName"]["uuid"]
                != e164number.trunk.name
            ):
                match = False
        if routepattern_get_response["return"]["routePattern"]["description"] != get_ucm_description(
            e164number.description
        ):
            match = False
        if routepattern_get_response["return"]["routePattern"]["blockEnable"] != "false":
            match = False
    except Exception as e:
        match = False

    if not match:
        e164number.synced_to_ucm = False
        e164number.save()
    else:
        e164number.synced_to_ucm = True
        e164number.date_last_verified = datetime.now()
        e164number.save()

    return match


def sync_e164_ucm_routepatterns(e164_logger, ucm_service, sync_device):
    """Send SOAP request. Check for Netbox E164Numbers on UCM and create if needed. Delete E164DeletedNumbers from UCM.
    """

    source = inspect.currentframe().f_code.co_name

    #######################################################################
    # Checking for deleted netbox numbers and deleting if necessary to UCM
    # Process delete queue first in case someone is trying to add it back to UCM
    #######################################################################
    nb_deleted_e164numbers = E164DeletedNumber.objects.filter(ucm=sync_device, deleted_on_ucm=False)

    for deleted_e164number in nb_deleted_e164numbers:
        e164_logger.sync_routepattern_info(
            source, f"Attempting deletion of {deleted_e164number.number} from {sync_device.name}"
        )
        # Checking for matching route-pattern on UCM
        if sync_ucm_e164_verify_number(e164_logger, ucm_service, deleted_e164number):
            delete_routepattern_response = ucm_service.removeRoutePattern(
                uuid="{" + deleted_e164number.ucm_uuid.__str__() + "}",
            )
            deleted_e164number.deleted_on_ucm = True
            deleted_e164number.deleted_on_ucm_date = datetime.now()
            deleted_e164number.save()
            e164_logger.sync_routepattern_info(
                source, f"Successful deletion of {deleted_e164number.number} from {sync_device.name}"
            )
        else:
            e164_logger.sync_routepattern_info(
                source, f"Deletion failed. No matching pattern {deleted_e164number.number} on {sync_device.name}"
            )

    ############################################################
    # Checking for netbox numbers and adding if necessary to UCM
    ############################################################

    nb_e164numbers = E164Number.objects.filter(ucm=sync_device)
    e164_logger.sync_routepattern_info(source, f"Collected {nb_e164numbers.__len__()} E164 numbers from Netbox")

    for e164number in nb_e164numbers:
        # initialize creation flag to false
        create_routepattern = False

        if e164number.ucm_uuid:
            try:
                routepattern_get_response = ucm_service.getRoutePattern(uuid=e164number.ucm_uuid)

                if not routepattern_get_response["return"]:
                    create_routepattern = True
                else:
                    create_routepattern = False
                    ##############################################################################
                    # UUID matches, check other parameters to make sure they haven't changed in UCM
                    ##############################################################################
                    if not sync_ucm_e164_verify_number(e164_logger, ucm_service, e164number):
                        # parameters are out of sync, push Netbox values back to UCM
                        try:
                            e164_logger.sync_routepattern_info(
                                source, f"E164Number {e164number.number} is out of sync in {sync_device.name}"
                            )
                            routepattern_update_response = ucm_service.updateRoutePattern(
                                uuid=e164number.ucm_uuid,
                                newPattern=get_ucm_pattern(e164number.number),
                                newRoutePartitionName=e164number.partition.name,
                                destination=get_ucm_destination(e164number),
                                description=get_ucm_description(e164number.description),
                                blockEnable=False,
                            )
                            e164number.date_last_verified = datetime.now()
                            e164number.synced_to_ucm = True
                            e164number.save()
                            e164_logger.sync_routepattern_info(
                                source,
                                f"Re-sync'd  E164Number {e164number.number} to Netbox values in {sync_device.name}",
                            )
                        except Exception as e:
                            e164_logger.sync_routepattern_info(
                                source,
                                f"Failed Re-sync  E164Number {e164number.number} Netbox values to {sync_device.name}",
                            )
                            e164_logger.sync_routepattern_info(source, e)
                    else:
                        # parameters are in sync
                        e164_logger.sync_routepattern_info(
                            source, f"Verified  E164Number {e164number.number} exists in {sync_device.name}"
                        )
            except Exception as e:
                if e.__str__() == "Item not valid: The specified RoutePattern was not found":
                    e164_logger.sync_routepattern_info(
                        source,
                        f"Previously synced E164Number {e164number.number} missing on {sync_device.name}. Attempting to recreate",
                    )
                    create_routepattern = True

        else:
            create_routepattern = True

        try:
            if create_routepattern:

                # attempt add
                e164_logger.sync_routepattern_info(
                    source, f"Adding E164Number {e164number.number} to {sync_device.name}"
                )
                routepattern_add_response = ucm_service.addRoutePattern(
                    routePattern={
                        "pattern": get_ucm_pattern(e164number.number),
                        "routePartitionName": e164number.partition.name,
                        "destination": get_ucm_destination(e164number),
                        "description": get_ucm_description(e164number.description),
                        "blockEnable": False,
                    }
                )
                e164number.ucm_uuid = routepattern_add_response["return"]
                e164number.date_last_verified = datetime.now()
                e164number.synced_to_ucm = True
                e164number.save()
                e164_logger.sync_routepattern_info(
                    source, f"Successfully added E164Number {e164number.number} to {sync_device.name}"
                )
        except Exception as e:
            e164_logger.sync_routepattern_info(
                source, f"Failed to add E164Number {e164number.number} to {sync_device.name}"
            )
            e164_logger.sync_routepattern_info(source, e)


def sync_e164_ucm(sync_device, master_key):
    """Get secret for api-role. Get service handle. Call sync functions for partitions, trunks, route-patterns, and update E164Task entries related to sync."""

    e164_logger = E164UcmTaskLogger(sync_device)

    try:
        secret = sync_device.secrets.get(role__slug="api-login")
    except Secret.DoesNotExist:
        message = f"Missing api-login secret for {sync_device.name}"
        source = inspect.currentframe().f_code.co_name
        return e164_logger.failed(source, message)

    secret.decrypt(master_key)

    try:
        ucm_service = get_ucm_service(sync_device.primary_ip.address.ip.__str__(), secret.name, secret.plaintext)
    except Exception as e:
        message = f"SOAP service creation failed for {sync_device.name}"
        source = "get_ucm_service"
        return e164_logger.failed(source, message)

    try:
        e164_logger.sync_partition_status(E164TaskStatus.STATUS_RUNNING)
        sync_e164_ucm_partitions(e164_logger, ucm_service, sync_device)
    except Exception as e:
        message = f"Sync partitions failed for {sync_device.name}"
        source = "sync_e164_ucm_partitions"
        e164_logger.sync_partition_info(source, message)
        e164_logger.sync_partition_info(source, e)
        e164_logger.sync_partition_status(E164TaskStatus.STATUS_FAILED)
    else:
        e164_logger.sync_partition_status(E164TaskStatus.STATUS_SUCCEEDED)

    try:
        e164_logger.sync_trunk_status(E164TaskStatus.STATUS_RUNNING)
        sync_e164_ucm_trunks(e164_logger, ucm_service, sync_device)
    except Exception as e:
        message = f"Sync trunks failed for {sync_device.name}"
        source = "sync_e164_ucm_trunks"
        e164_logger.sync_trunk_info(source, message)
        e164_logger.sync_trunk_info(source, e)
        e164_logger.sync_trunk_status(E164TaskStatus.STATUS_FAILED)
    else:
        e164_logger.sync_trunk_status(E164TaskStatus.STATUS_SUCCEEDED)

    try:
        e164_logger.sync_routepattern_status(E164TaskStatus.STATUS_RUNNING)
        sync_e164_ucm_routepatterns(e164_logger, ucm_service, sync_device)
    except Exception as e:
        message = f"Sync route-patterns failed for {sync_device.name}"
        source = "sync_e164_ucm_routepatterns"
        e164_logger.sync_routepattern_info(source, message)
        e164_logger.sync_routepattern_info(source, e)
        e164_logger.sync_routepattern_status(E164TaskStatus.STATUS_FAILED)
    else:
        e164_logger.sync_routepattern_status(E164TaskStatus.STATUS_SUCCEEDED)

    return {"status": "succeeded"}


def get_cube_e164_pattern_map(e164_logger, napalm_device, map_entry=None):
    """Execute command on CUBE and return list of dictionaries with output"""

    source = inspect.currentframe().f_code.co_name

    if map_entry is None:
        cli_e164_status = "show voice class e164-pattern-map"
    else:
        cli_e164_status = f"show voice class e164-pattern-map {map_entry}"

    cli_commands = [cli_e164_status]
    e164_status = napalm_device.cli(cli_commands)

    # Creates template from TextFSM template file
    with open(f"{dirname(__file__)}/show_voice_e164_pattern_map.textfsm", "r") as f:
        template = textfsm.TextFSM(f)

    # Parses output
    e164_status_list = template.ParseText(e164_status[cli_e164_status])

    # Creates list and transforms csv array into list of dicts
    list_of_e164 = []
    for pattern_map in e164_status_list:
        list_of_e164.append(dict(zip(template.header, pattern_map)))

    return list_of_e164


def sync_e164_cube(sync_device, master_key):
    """Get secret for api-role. Get Session handle. Call sync functions for e164patternmap"""

    e164_logger = E164CubeTaskLogger(sync_device)
    source = inspect.currentframe().f_code.co_name

    try:
        secret = sync_device.secrets.get(role__slug="ssh-login")
    except Secret.DoesNotExist:
        message = f"Missing ssh-login secret for {sync_device.name}"
        source = inspect.currentframe().f_code.co_name
        return e164_logger.failed(source, message)

    secret.decrypt(master_key)

    e164_logger.sync_cube_status(E164TaskStatus.STATUS_RUNNING)

    napalm_driver = get_network_driver("ios")
    if not sync_device.primary_ip:
        e164_logger.sync_cube_status(E164TaskStatus.STATUS_FAILED)
        e164_logger.sync_cube_info(source, f"Device {sync_device.name} missing primary IP.")
        return {"status": "failed"}

    try:
        napalm_device = napalm_driver(sync_device.primary_ip.address.ip.__str__(), secret.name, secret.plaintext)
        napalm_device.open()
        e164_logger.sync_cube_info(source, f"Device {sync_device.name} SSH connection successful.")
    except Exception as e:
        e164_logger.sync_cube_status(E164TaskStatus.STATUS_FAILED)
        e164_logger.sync_cube_info(source, f"Device {sync_device.name} SSH connection failed.")
        e164_logger.sync_cube_info(source, e)
        return {"status": "failed"}

    list_of_e164 = get_cube_e164_pattern_map(e164_logger, napalm_device)

    # Cycle through e164-pattern-map entries looking for ones that point to us
    for pattern_map in list_of_e164:
        try:
            if pattern_map["SOURCE"] == "url" and pattern_map["URL"].index("/api/plugins/e164/e164PatternMap?tenant="):
                e164_logger.sync_cube_info(
                    source, f"Refreshing map {pattern_map['E164_PATTERN_MAP']} with {pattern_map['ENTRIES']} entry"
                )
                sync_cube_e164_pattern_map(e164_logger, napalm_device, pattern_map["E164_PATTERN_MAP"])
        except ValueError:
            pass

    e164_logger.sync_cube_status(E164TaskStatus.STATUS_SUCCEEDED)
    return {"status": "succeeded"}


def sync_cube_e164_pattern_map(e164_logger, napalm_device, map_entry):
    """Execute command on CUBE and log status of sync"""

    source = inspect.currentframe().f_code.co_name

    cli_e164_load = f"voice class e164-pattern-map load {map_entry}"

    cli_commands = [cli_e164_load]
    e164_load = napalm_device.cli(cli_commands)

    # Creates template from TextFSM template file
    with open(f"{dirname(__file__)}/voice_class_e164_pattern_map_load.textfsm", "r") as f:
        template = textfsm.TextFSM(f)

    # Parses output
    e164_load_status = template.ParseText(e164_load[cli_e164_load])

    # Converts to dict
    e164_load_status_dict = dict(zip(template.header, e164_load_status[0]))

    if e164_load_status_dict["URL_STATUS"] == "successfully" and e164_load_status_dict["PATTERNS_VALID"] == "valid":
        e164_logger.sync_cube_info(source, f"Map loaded successfully")
        e164_pattern_list_status = get_cube_e164_pattern_map(e164_logger, napalm_device, map_entry)
        e164_logger.sync_cube_info(source, f"E164 Pattern Map Entry Count: {e164_pattern_list_status[0]['ENTRIES']}")
    else:
        e164_logger.sync_cube_info(
            source,
            f"Map load failure. URL Status: {e164_load_status_dict['URL_STATUS']}, Patterns Status: {e164_load_status_dict['PATTERNS_VALID']}",
        )
        if e164_load_status_dict["PATTERNS_VALID"] == "Error":
            e164_pattern_list_status = get_cube_e164_pattern_map(e164_logger, napalm_device, map_entry)
            e164_logger.sync_cube_info(
                source, f"E164 Pattern Map Parsing Error: {e164_pattern_list_status[0]['PARSING_ERROR']}"
            )
        e164_logger.sync_cube_status(E164TaskStatus.STATUS_FAILED)
        raise Exception

    # Creates list and transforms csv array into list of dicts
    # list_of_e164=[]
    # for pattern_map in e164_status_list:
    #     list_of_e164.append(dict(zip(template.header, pattern_map)))

    # e164_logger.sync_cube_info(source, f"Collected {list_of_e164.count} e164-pattern-maps")

    # return list_of_e164


class E164CubeTaskLogger:

    sync_cube_task = None

    def __init__(self, sync_device):
        self.sync_cube_task = E164Task.objects.create(
            type=E164TaskTypes.CUBE_E164MAP_SYNC, device=sync_device, status=E164TaskStatus.STATUS_PENDING
        )

    def sync_cube_info(self, source, message):
        E164TaskLogEntry.objects.create(task=self.sync_cube_task, source=source, message=message)

    def sync_cube_status(self, status):
        """:status as .choices.E164TaskStatus"""

        self.sync_cube_task.status = status
        self.sync_cube_task.save()

    def failed(self, source, message):
        self.sync_cube_info(source, message)
        self.sync_cube_status(E164TaskStatus.STATUS_FAILED)


class E164UcmTaskLogger:

    sync_partition_task = None
    sync_trunk_task = None
    sync_routepattern_task = None

    def __init__(self, sync_device):
        self.sync_partition_task = E164Task.objects.create(
            type=E164TaskTypes.UCM_PARTITION_SYNC, device=sync_device, status=E164TaskStatus.STATUS_PENDING
        )
        self.sync_trunk_task = E164Task.objects.create(
            type=E164TaskTypes.UCM_TRUNK_SYNC, device=sync_device, status=E164TaskStatus.STATUS_PENDING
        )
        self.sync_routepattern_task = E164Task.objects.create(
            type=E164TaskTypes.UCM_ROUTEPATTERN_SYNC, device=sync_device, status=E164TaskStatus.STATUS_PENDING
        )

    def sync_partition_info(self, source, message):
        E164TaskLogEntry.objects.create(task=self.sync_partition_task, source=source, message=message)

    def sync_partition_status(self, status):
        """:status as .choices.E164TaskStatus"""

        self.sync_partition_task.status = status
        self.sync_partition_task.save()

    def sync_trunk_info(self, source, message):
        E164TaskLogEntry.objects.create(task=self.sync_trunk_task, source=source, message=message)

    def sync_trunk_status(self, status):
        """:status as .choices.E164TaskStatus"""

        self.sync_trunk_task.status = status
        self.sync_trunk_task.save()

    def sync_routepattern_info(self, source, message):
        E164TaskLogEntry.objects.create(task=self.sync_routepattern_task, source=source, message=message)

    def sync_routepattern_status(self, status):
        """:status as .choices.E164TaskStatus"""

        self.sync_routepattern_task.status = status
        self.sync_routepattern_task.save()

    def failed(self, source, message):
        self.sync_partition_info(source, message)
        self.sync_trunk_info(source, message)
        self.sync_routepattern_info(source, message)

        self.sync_partition_status(E164TaskStatus.STATUS_FAILED)
        self.sync_trunk_status(E164TaskStatus.STATUS_FAILED)
        self.sync_routepattern_status(E164TaskStatus.STATUS_FAILED)

        return {"status": "failed", "message": message}
