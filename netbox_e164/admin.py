"""Administrative capabilities for netbox_e164 plugin.

(c) 2020 C Spire Business
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from django.contrib import admin
from .models import E164Number, E164DeletedNumber, E164Partition, E164Trunk, E164Task, E164TaskLogEntry


@admin.register(E164Number)
class E164NumberAdmin(admin.ModelAdmin):
    """Administrative view for managing E164Number instances."""

    list_display = (
        "pk",
        "created",
        "number",
        "ucm_uuid",
        "site",
        "tenant",
        "ucm",
        "partition",
        "description",
        "synced_to_ucm",
        "date_last_verified",
    )


@admin.register(E164DeletedNumber)
class E164DeletedNumberAdmin(admin.ModelAdmin):
    """Administrative view for managing E164DeletedNumber instances."""

    list_display = (
        "pk",
        "created",
        "number",
        "ucm_uuid",
        "site",
        "tenant",
        "ucm",
        "partition",
        "trunk",
        "description",
        "deleted_on_ucm",
        "deleted_on_ucm_date",
    )


@admin.register(E164Partition)
class E164PartitionAdmin(admin.ModelAdmin):
    """Administrative view for managing E164Partition instances."""

    list_display = (
        "pk",
        "created",
        "name",
        "ucm",
        "ucm_uuid",
    )


@admin.register(E164Trunk)
class E164TrunkAdmin(admin.ModelAdmin):
    """Administrative view for managing E164Trunk instances."""

    list_display = (
        "pk",
        "created",
        "name",
        "type",
        "ucm",
        "ucm_uuid",
    )


@admin.register(E164Task)
class E164TaskAdmin(admin.ModelAdmin):
    """Administrative view for managing E164Task instances."""

    list_display = (
        "pk",
        "created_on",
        "device",
        "type",
        "status",
    )


@admin.register(E164TaskLogEntry)
class E164TaskLogEntryAdmin(admin.ModelAdmin):
    """Administrative view for managing E164TaskLogEntry instances."""

    list_display = (
        "pk",
        "created_on",
        "task",
        "source",
        "message",
    )
