from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from .models import E164Number, E164DeletedNumber


@receiver(pre_delete, sender=E164Number)
def save_deleted_number(instance, **kwargs):
    """When a number is deleted, store a copy in the deleted number table."""

    deleted_number = E164DeletedNumber.objects.create(
        number=instance.number,
        tenant=instance.tenant,
        site=instance.site,
        description=instance.description,
        ucm=instance.ucm,
        partition=instance.partition,
        trunk=instance.trunk,
        ucm_uuid=instance.ucm_uuid,
    )
