"""Django urlpatterns declaration for netbox_e164 plugin.

(c) 2020 C Spire Business
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from django.urls import path, re_path

from .apps import E164AppConfig

from .views import (
    E164NumberListView,
    E164NumberView,
    E164NumberEditView,
    E164NumberBulkDeleteView,
    E164NumberBulkImportView,
    load_partitions,
    load_trunks,
    E164TaskListView,
    E164TaskLogEntryView,
    E164TaskLogEntryViewFiltered,
    E164TaskBulkDeleteView,
    E164SyncRequestView,
    sync_e164,
)

urlpatterns = [
    path("", E164NumberListView.as_view(), name="e164number_list"),
    path("add/", E164NumberView.as_view(), name="e164number_add"),
    path("<int:pk>/edit/", E164NumberEditView.as_view(), name="e164number_edit"),
    path("delete/", E164NumberBulkDeleteView.as_view(), name="e164number_delete"),
    path("import/", E164NumberBulkImportView.as_view(), name="e164number_import"),
    path("ajax/load-partitions/", load_partitions, name="ajax_load_partitions"),
    path("ajax/load-trunks/", load_trunks, name="ajax_load_trunks"),
    path("task/", E164TaskListView.as_view(), name="e164task_list"),
    path("task/delete", E164TaskBulkDeleteView.as_view(), name="e164task_delete"),
    path("task/add", E164SyncRequestView.as_view(), name="e164syncrequest_add"),
    path("task/logentry/", E164TaskLogEntryView.as_view(), name="e164tasklogentry_list"),
    re_path(r"task/logentry/\?task=/d*", E164TaskLogEntryViewFiltered.as_view(), name="e164tasklogentryfiltered_list"),
    path("ajax/sync-ucm/", sync_e164, name="ajax_sync_e164"),
]

# plugin init code, nowhere else to put it for now
E164AppConfig.ready()
