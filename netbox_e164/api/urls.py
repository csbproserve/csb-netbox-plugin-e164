"""REST API URLs for E164Numbers.

(c) 2020 C Spire Business
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from django.urls import path
from rest_framework import routers
from .views import E164NumberView, exportViewToDjango, E164PartitionViewSet, E164TrunkViewSet

router = routers.DefaultRouter()

router.register(r"numbers", E164NumberView)
router.register(r"partitions", E164PartitionViewSet)
router.register(r"trunk", E164TrunkViewSet)


urlpatterns = router.urls

urlpatterns.append(path("e164PatternMap", exportViewToDjango, name="netbox_e164_export_map"))
