"""Django REST Framework API views for e164 number management.

(c) 2020 C Spire Business
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from rest_framework import mixins, viewsets
from django.http import HttpResponse

from extras.api.views import CustomFieldModelViewSet
from netbox.api.views import ModelViewSet

from netbox_e164.models import E164Number, E164Partition, E164Trunk
from netbox_e164.filters import E164NumberListFilter, E164PartitionListFilter, E164TrunkListFilter

from .serializers import E164NumberSerializer, E164PartitionSerializer, E164TrunkSerializer


class E164NumberView(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    """Create, List, Get, Update, Delete E164Number instances."""

    queryset = E164Number.objects.all()
    filterset_class = E164NumberListFilter
    serializer_class = E164NumberSerializer


class E164PartitionView(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    """Create, List, Get, Update, Delete E164Partition instances."""

    queryset = E164Partition.objects.all()
    filterset_class = E164PartitionListFilter
    serializer_class = E164PartitionSerializer


def buildExport(tenant):
    """Build and format export string"""
    patternMap = ""
    for e164 in E164Number.objects.filter(tenant__slug=tenant):
        if len(patternMap) == 0:
            patternMap = e164.number.replace("X", ".")
        else:
            patternMap = patternMap + "\n" + e164.number.replace("X", ".")
    return patternMap


def exportViewToDjango(request):
    """Export built view to Django"""
    tenant = request.GET["tenant"]
    return HttpResponse(buildExport(tenant), content_type="text/plain")


class E164PartitionViewSet(ModelViewSet):
    queryset = E164Partition.objects.prefetch_related("ucm")
    # ).order_by(*E164Partition.Meta.ordering)
    serializer_class = E164PartitionSerializer
    filterset_class = E164PartitionListFilter


class E164TrunkViewSet(ModelViewSet):
    queryset = E164Trunk.objects.prefetch_related("ucm")
    # ).order_by(*E164Partition.Meta.ordering)
    serializer_class = E164TrunkSerializer
    filterset_class = E164TrunkListFilter
