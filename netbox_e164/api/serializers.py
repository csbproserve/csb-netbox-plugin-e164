"""Model serializers for the netbox_e164 REST API.

(c) 2020 C Spire Business
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from rest_framework import serializers
from django_rq import get_queue

from dcim.models import Site
from tenancy.models import Tenant
from dcim.api.nested_serializers import NestedSiteSerializer, NestedDeviceSerializer
from tenancy.api.nested_serializers import NestedTenantSerializer

from netbox_e164.models import E164Number, E164Partition, E164Trunk


class E164PartitionSerializer(serializers.ModelSerializer):
    """Serializer for the E164Partition model."""

    name = serializers.CharField(required=True, help_text="Name of Partition on UCM")

    ucm = NestedDeviceSerializer()

    class Meta:
        model = E164Partition
        fields = ["pk", "name", "ucm", "created"]


class E164TrunkSerializer(serializers.ModelSerializer):
    """Serializer for the E164Partition model."""

    name = serializers.CharField(required=True, help_text="Name of Partition on UCM")

    ucm = NestedDeviceSerializer()

    class Meta:
        model = E164Trunk
        fields = ["pk", "name", "ucm", "created"]


class E164NumberSerializer(serializers.ModelSerializer):
    """Serializer for the E164Number model."""

    site = NestedSiteSerializer()

    tenant = NestedTenantSerializer()

    number = serializers.CharField(required=True, help_text="Number in E.164 format (including +)",)

    description = serializers.CharField(required=False, write_only=True, help_text="Short description of number usage",)

    ucm = NestedDeviceSerializer(required=False, allow_null=True)

    partition = E164PartitionSerializer()

    trunk = E164TrunkSerializer()

    class Meta:
        model = E164Number
        fields = [
            "pk",
            "number",
            "site",
            "tenant",
            "description",
            "ucm",
            "partition",
            "trunk",
            "created",
        ]
