"""Plugin declaration for netbox_e164.

(c) 2020 C Spire Business
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

__version__ = "0.1.0"

import logging

from extras.plugins import PluginConfig


class E164Config(PluginConfig):
    """Plugin configuration for the netbox_e164 plugin."""

    name = "netbox_e164"
    verbose_name = "E164 Management"
    version = __version__
    author = "C Spire Business"
    description = "A plugin for NetBox to manage E164 numbers."
    base_url = "e164"
    required_settings = []
    min_version = "2.8.1"
    default_settings = {"ucm_role": "ucm", "cube_role": "cube", "ucm_routepattern_desc_prefix": "nbE164-"}
    caching_config = {}
    # default_app_config = 'netbox_e164.apps.E164AppConfig'


config = E164Config  # pylint:disable=invalid-name
