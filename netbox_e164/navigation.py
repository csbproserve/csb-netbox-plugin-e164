"""Plugin additions to the NetBox navigation menu.

(c) 2020 C Spire Business
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from extras.plugins import PluginMenuButton, PluginMenuItem
from utilities.choices import ButtonColorChoices

menu_items = (
    PluginMenuItem(
        link="plugins:netbox_e164:e164number_list",
        link_text="E164 Numbers",
        permissions=["netbox_e164.view_e164numbers"],
        buttons=(
            PluginMenuButton(
                link="plugins:netbox_e164:e164number_add",
                title="Add",
                icon_class="mdi mdi-plus-thick",
                color=ButtonColorChoices.GREEN,
                permissions=["netbox_e164.add_e164number"],
            ),
            PluginMenuButton(
                link="plugins:netbox_e164:e164number_import",
                title="Bulk Import",
                icon_class="mdi mdi-database-import-outline",
                color=ButtonColorChoices.CYAN,
                permissions=["netbox_e164.add_e164number"],
            ),
        ),
    ),
    PluginMenuItem(
        link="plugins:netbox_e164:e164task_list",
        link_text="E164 Tasks",
        permissions=["netbox_e164.view_e164tasks"],
        buttons=(
            PluginMenuButton(
                link="plugins:netbox_e164:e164syncrequest_add",
                title="Add",
                icon_class="mdi mdi-plus-thick",
                color=ButtonColorChoices.GREEN,
                permissions=["netbox_e164.add_e164task"],
            ),
        ),
    ),
)
