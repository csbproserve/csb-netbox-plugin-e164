"""Django views for e164 management.

(c) 2020 C Spire Business
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import logging
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import render
from django.conf import settings
from django.db.models import Q
from django_rq import get_queue

from netbox.views.generic import BulkDeleteView, BulkImportView, ObjectEditView, ObjectListView
from dcim.models import Device

from .filters import E164NumberListFilter, E164TaskListFilter, E164TaskLogEntryFilter
from .forms import E164NumberListFilterForm, E164TaskListFilterForm, E164TaskLogEntryFilterForm
from .forms import E164NumberForm
from .forms import E164NumberCSVForm
from .models import E164Number, E164Partition, E164Trunk, E164Task, E164TaskLogEntry
from .tables import E164NumberTable, E164NumberBulkTable, E164TaskTable, E164TaskLogEntryTable, E164SyncRequestTable
from .utils.ucm import NetboxUcm

PLUGIN_SETTINGS = settings.PLUGINS_CONFIG["netbox_e164"]

log = logging.getLogger("rq.worker")
log.setLevel(logging.DEBUG)


class E164NumberListView(PermissionRequiredMixin, ObjectListView):
    """View for listing all E164 Numbers."""

    permission_required = "netbox_e164.view_e164numberlist"
    queryset = E164Number.objects.all().order_by("-id")
    filterset = E164NumberListFilter
    filterset_form = E164NumberListFilterForm
    table = E164NumberTable
    template_name = "netbox_e164/e164_number_list.html"
    action_buttons = (
        "add",
        "import",
        "export",
    )


class E164NumberView(PermissionRequiredMixin, ObjectEditView):
    """View for creating an E164 Number. Checks for UCM device role and creates if necessary."""

    permission_required = "netbox_e164.add_e164number"
    model = E164Number
    queryset = E164Number.objects.all()
    model_form = E164NumberForm
    template_name = "netbox_e164/e164_number_edit.html"
    default_return_url = "plugins:netbox_e164:e164number_list"


class E164NumberEditView(E164NumberView):
    """View for editing an E164 Number."""

    permission_required = "netbox_e164.edit_e164number"


class E164NumberBulkDeleteView(PermissionRequiredMixin, BulkDeleteView):
    """View for deleting one or more E164 Numbers."""

    permission_required = "netbox_e164.delete_e164number"
    queryset = E164Number.objects.filter()
    table = E164NumberTable
    default_return_url = "plugins:netbox_e164:e164number_list"


class E164NumberBulkImportView(PermissionRequiredMixin, BulkImportView):
    """View for bulk-importing a CSV file to create OnboardingTasks."""

    permission_required = "netbox_e164.add_e164number"
    queryset = E164Number.objects.all()
    model_form = E164NumberCSVForm
    table = E164NumberBulkTable
    default_return_url = "plugins:netbox_e164:e164number_list"


class E164TaskListView(PermissionRequiredMixin, ObjectListView):
    """View for status of E164Task instances and queueing new tasks"""

    permission_required = "netbox_e164.view_e164task"
    queryset = E164Task.objects.all()
    filterset = E164TaskListFilter
    filterset_form = E164TaskListFilterForm
    table = E164TaskTable
    template_name = "netbox_e164/e164_task_list.html"
    action_buttons = (
        "add",
        "import",
        "export",
    )


class E164TaskLogEntryView(PermissionRequiredMixin, ObjectListView):
    """View for log entries for E164Tasks"""

    permission_required = "netbox_e164.view_e164tasklogentry"
    queryset = E164TaskLogEntry.objects.filter()
    filterset = E164TaskLogEntryFilter
    filterset_form = E164TaskLogEntryFilterForm
    table = E164TaskLogEntryTable
    template_name = "netbox_e164/e164_task_logentry.html"


class E164TaskLogEntryViewFiltered(PermissionRequiredMixin, ObjectListView):
    """View for log entries for E164Tasks"""

    permission_required = "netbox_e164.view_e164tasklogentry"
    filterset = E164TaskLogEntryFilter
    filterset_form = E164TaskLogEntryFilterForm
    table = E164TaskLogEntryTable
    template_name = "netbox_e164/e164_task_logentry.html"

    def alter_queryset(self, request):
        task_id = request.GET.get("task")
        queryset = E164TaskLogEntry.objects.filter(task__id=task_id)
        return queryset


class E164SyncRequestView(PermissionRequiredMixin, ObjectListView):
    """View to request sync for UCM and CUBE"""

    permission_required = "netbox_e164.add_e164task"
    ucm_role = PLUGIN_SETTINGS["ucm_role"]
    cube_role = PLUGIN_SETTINGS["cube_role"]
    syncrequest_filter = Q(device_role__slug__exact=ucm_role) | Q(device_role__slug__exact=cube_role)
    queryset = Device.objects.filter(syncrequest_filter)
    table = E164SyncRequestTable
    template_name = "netbox_e164/e164_syncrequest.html"
    default_return_url = "plugins:netbox_e164:e164task_list"


class E164TaskBulkDeleteView(PermissionRequiredMixin, BulkDeleteView):
    """View for deleting one or more E164 Tasks."""

    permission_required = "netbox_e164.delete_e164task"
    queryset = E164Task.objects.filter()
    table = E164TaskTable
    default_return_url = "plugins:netbox_e164:e164task_list"


def load_partitions(request):
    ucm_id = request.GET.get("ucm")
    partitions = E164Partition.objects.filter(ucm__id=ucm_id).order_by("name")
    return render(request, "netbox_e164/partition_dropdown_list_options.html", {"partitions": partitions})


def load_trunks(request):
    ucm_id = request.GET.get("ucm")
    trunks = E164Trunk.objects.filter(ucm__id=ucm_id).order_by("name")
    return render(request, "netbox_e164/trunk_dropdown_list_options.html", {"trunks": trunks})


def sync_e164(request):
    dev_id = request.GET.get("device_id")
    sync_device = Device.objects.filter(id=dev_id)[0]

    get_queue("default").enqueue("netbox_e164.worker.sync_e164", sync_device.id)

    return render(request, "netbox_e164/sync_e164.html", {"dev_name": sync_device.name})
