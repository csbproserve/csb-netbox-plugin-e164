import logging

from django.conf import settings
from django.apps import AppConfig

from dcim.models import Manufacturer, DeviceType, Device, InterfaceTemplate, Site, DeviceRole
from ipam.models import IPAddress
from secrets.models import Secret
from tenancy.models import Tenant
from .utils.ucm import NetboxUcm
from .utils.secrets import E164Secrets
from .models import E164Number

PLUGIN_SETTINGS = settings.PLUGINS_CONFIG["netbox_e164"]
ucm_role = PLUGIN_SETTINGS["ucm_role"]
cube_role = PLUGIN_SETTINGS["cube_role"]


class E164AppConfig(AppConfig):
    """Create database rows required by E164Number Plugin"""

    name = "netbox_e164.E164Config"
    verbose_name = "CSB Netbox E164 Plugin"
    # def ready(self):
    def ready():
        try:
            import netbox_e164.signals

            NetboxUcm.check_create_ucm_role()
            NetboxUcm.check_create_secret_role()
            NetboxUcm.check_create_cube_role()
        except Exception as e:
            logging.warning("database not initialized - E164 roles not created")

        if settings.DEVELOPER:
            # create test data
            try:
                mfg_cisco = Manufacturer.objects.create(name="Cisco", slug="cisco")
                dev_type_ucm = DeviceType.objects.create(
                    model="Unified Communications Manager",
                    slug="cucm",
                    manufacturer=Manufacturer.objects.get(slug="cisco"),
                )
                dev_type_cube = DeviceType.objects.create(
                    model="ISR4431", slug="isr4431", manufacturer=Manufacturer.objects.get(slug="cisco")
                )
                ucm_if = InterfaceTemplate.objects.create(name="Eth0", device_type=DeviceType.objects.get(slug="cucm"))
                cube_if = InterfaceTemplate.objects.create(
                    name="G0/0/0", device_type=DeviceType.objects.get(slug="isr4431")
                )
                colo = Site.objects.create(name="Colo", slug="colo")
                ucm1 = Device.objects.create(
                    name="ColoUCM",
                    device_role=DeviceRole.objects.get(slug=ucm_role),
                    device_type=DeviceType.objects.get(slug="cucm"),
                    site=Site.objects.get(name="Colo"),
                )
                ucm1_int = ucm1.interfaces.get(name="Eth0")
                ucm1_ip = ucm1_int.ip_addresses.create(address="10.4.50.10/32")
                cube1 = Device.objects.create(
                    name="ColoCUBE",
                    device_role=DeviceRole.objects.get(slug=cube_role),
                    device_type=DeviceType.objects.get(slug="isr4431"),
                    site=Site.objects.get(name="Colo"),
                )
                cube1_int = cube1.interfaces.get(name="G0/0/0")
                cube1_ip = cube1_int.ip_addresses.create(address="10.4.50.20/32")
                tenant1 = Tenant.objects.create(name="C Spire Business", slug="csb")

            except:
                pass
