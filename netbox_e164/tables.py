"""Tables for E164 Numbers.

(c) 2020 C Spire Business
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import django_tables2 as tables
from django.conf import settings

from utilities.tables import BaseTable, ToggleColumn
from dcim.models import Device
from .models import E164Number, E164Task, E164TaskLogEntry

PLUGIN_SETTINGS = settings.PLUGINS_CONFIG["netbox_e164"]


class E164NumberTable(BaseTable):
    """Table for displaying E164Number instances."""

    pk = ToggleColumn()
    number = tables.Column(linkify=True)
    site = tables.LinkColumn()
    tenant = tables.LinkColumn()

    class Meta(BaseTable.Meta):
        model = E164Number
        fields = (
            "pk",
            "created",
            "number",
            "site",
            "tenant",
            "description",
            "synced_to_ucm",
            "date_last_verified",
        )


class E164NumberBulkTable(BaseTable):
    """Table for importing multiple E164 numbers at once."""

    site = tables.LinkColumn()
    tenant = tables.LinkColumn()

    class Meta(BaseTable.Meta):  # noqa: D106 "Missing docstring in public nested class"
        model = E164Number
        fields = (
            "pk",
            "created",
            "number",
            "site",
            "tenant",
            "description",
        )


class E164TaskTable(BaseTable):
    """Table for displaying E164Task instances."""

    pk = ToggleColumn()
    created_on = tables.DateTimeColumn()
    device = tables.LinkColumn()
    type = tables.Column()
    status = tables.Column()
    logs = tables.TemplateColumn(template_name="netbox_e164/e164_task_list_logbutton.html",)

    class Meta(BaseTable.Meta):
        model = E164Task
        fields = (
            "pk",
            "created_on",
            "device",
            "type",
            "status",
        )


class E164TaskLogEntryTable(BaseTable):
    """Table for displaying E164TaskLogEntry Instances."""

    created_on = tables.DateTimeColumn()
    task = tables.Column()
    source = tables.Column()
    message = tables.Column()

    class Meta(BaseTable.Meta):
        model = E164TaskLogEntry
        fields = (
            "created_on",
            "task",
            "source",
            "message",
        )


class E164SyncRequestTable(BaseTable):
    """Table for displaying devices available for E164 Sync Requests"""

    ucm_role = PLUGIN_SETTINGS["ucm_role"]
    cube_role = PLUGIN_SETTINGS["cube_role"]

    pk = ToggleColumn()
    name = tables.LinkColumn()
    device_role = tables.LinkColumn()
    sync = tables.TemplateColumn(
        template_code="""
    <a onClick="ajax_sync_e164({{record.pk}})" class="btn btn-default btn-xs" title="Sync">
        <i class="mdi mdi-database-sync"></i>
    </a>
    """
    )

    class Meta(BaseTable.Meta):
        model = Device
        fields = (
            "pk",
            "name",
            "device_role",
        )
