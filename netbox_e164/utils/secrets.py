import logging

from secrets.models import UserKey, SessionKey


class E164Secrets:
    """Used to get session key"""

    def getSessionKey(username, private_key):
        """Check for existing session, then create one if it isn't found"""

        try:
            user_key = UserKey.objects.get(user__username=username)
        except UserKey.DoesNotExist:
            logging.warning(f"UserKey not found for {username}")
            return None
        if not user_key.is_active():
            logging.warning(f"UserKey inactive for {username}")
            return None

        master_key = user_key.get_master_key(private_key)
        if master_key is None:
            logging.warning(f"Private key invalid for user {username}")

        try:
            current_session_key = SessionKey.objects.get(userkey__user__username=username)
        except SessionKey.DoesNotExist:
            current_session_key = None

        if current_session_key:
            key = current_session_key.get_session_key(master_key)
        else:
            sk = SessionKey(userkey=user_key)
            sk.save(master_key=master_key)
            key = sk.key

        return key

    def getMasterKey(username, session_key):
        """Return master key from session"""

        try:
            master_key = SessionKey.objects.get(userkey__user__username=username).get_master_key(session_key)
        except SessionKey.DoesNotExist:
            logging.warning(f"Session not found for user {username}")
        else:
            if master_key is None:
                logging.warning(f"Session key invalid for user {username}")

        return master_key
