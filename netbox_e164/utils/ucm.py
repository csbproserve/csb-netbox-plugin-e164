"""Worker code for syncing route-patterns on UCM and updating CUBE pattern-maps.

(c) 2020 C Spire Business
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import logging
import re

from django.conf import settings
from dcim.models import DeviceRole
from secrets.models import SecretRole

PLUGIN_SETTINGS = settings.PLUGINS_CONFIG["netbox_e164"]

# class UcmSync:
# """Sync's route-patterns, partitions, and trunks from UCM"""


class NetboxUcm:
    """Class to deal w/ UCM records in Netbox"""

    def check_create_ucm_role():
        """Get ucm_role from settings, conform to slug characters, check for existence in DeviceRoles, create if needed."""
        ucm_role = PLUGIN_SETTINGS["ucm_role"]
        slug = ucm_role
        if re.search(r"[^a-zA-Z0-9\-_]+", slug):
            logging.warning("ucm role is not sluggable: %s", slug)
            ucm_role = slug.replace(" ", "-")
            PLUGIN_SETTINGS["ucm_role"] = ucm_role
            logging.warning("ucm role is now: %s", ucm_role)

        try:
            DeviceRole.objects.create(name=ucm_role, description="Cisco Unified Communications Manger", slug=ucm_role)
        except:
            pass

    def check_create_secret_role():
        """Create secret roles if not existing"""

        try:
            SecretRole.objects.create(name="api-login", slug="api-login")
        except:
            pass

        try:
            SecretRole.objects.create(name="ssh-login", slug="ssh-login")
        except:
            pass

    def check_create_cube_role():
        """Create cube role if not existing"""

        cube_role = PLUGIN_SETTINGS["cube_role"]
        slug = cube_role
        if re.search(r"[^a-zA-Z0-9\-_]+", slug):
            logging.warning("cube role is not sluggable: %s", slug)
            cube_role = slug.replace(" ", "-")
            PLUGIN_SETTINGS["cube_role"] = cube_role
            logging.warning("cube role is now: %s", cube_role)

        try:
            DeviceRole.objects.create(name=cube_role, description="Cisco CUBE", slug=cube_role)
        except:
            pass


# class CubeUpdate:
