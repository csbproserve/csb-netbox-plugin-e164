"""E164Number Django model.

(c) 2020 C Spire Business
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from django.db import models
from django.core.validators import RegexValidator
from extras.utils import extras_features
from netbox.models import PrimaryModel
from utilities.querysets import RestrictedQuerySet
from netbox_e164.choices import E164TaskStatus, E164TaskTypes, E164TrunkTypes


@extras_features("export_templates", "webhooks")
class E164Number(PrimaryModel):
    """Used to track individual phone numbers."""

    number = models.CharField(
        unique=True, null=False, max_length=50, help_text="Enter number in E.164 format (including +)"
    )

    ucm_uuid = models.UUIDField(unique=True, blank=True, null=True)

    tenant = models.ForeignKey(to="tenancy.Tenant", on_delete=models.PROTECT, blank=True, null=True)

    site = models.ForeignKey(to="dcim.Site", on_delete=models.PROTECT, blank=True, null=True)

    description = models.TextField(blank=True, null=True)

    ucm = models.ForeignKey(to="dcim.Device", on_delete=models.PROTECT, blank=True, null=True)

    partition = models.ForeignKey(to="netbox_e164.E164Partition", on_delete=models.PROTECT, blank=True, null=True)

    trunk = models.ForeignKey(to="netbox_e164.E164Trunk", on_delete=models.PROTECT, blank=True, null=True)

    synced_to_ucm = models.BooleanField(default=False)

    date_last_verified = models.DateTimeField(blank=True, null=True)

    validators = [
        RegexValidator(
            regex=r"^\+?[1-9][\d\[\]-]{1,14}$",
            message="Enter a valid E164 number including the + sign. Follow UCM Wildcard conventions.",
        )
    ]

    objects = RestrictedQuerySet.as_manager()

    csv_headers = [
        "number",
        "tenant",
        "site",
        "description",
        "ucm",
        "partition",
        "trunk",
    ]

    class Meta:
        """Provides default order for class"""

        ordering = ["number"]

    def __str__(self):
        """String representation of an E164 Number"""
        return f"{self.site} : {self.number}"

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("plugins:netbox_e164:e164number_edit", args=[self.pk])


class E164DeletedNumber(PrimaryModel):
    """Used to track individual phone numbers as they are deleted and removed from UCM."""

    number = models.CharField(
        unique=False, null=False, max_length=50, help_text="Enter number in E.164 format (including +)"
    )

    ucm_uuid = models.UUIDField(blank=True, null=True)

    tenant = models.ForeignKey(to="tenancy.Tenant", on_delete=models.SET_NULL, blank=True, null=True)

    site = models.ForeignKey(to="dcim.Site", on_delete=models.SET_NULL, blank=True, null=True)

    description = models.TextField(blank=True, null=True)

    ucm = models.ForeignKey(to="dcim.Device", on_delete=models.SET_NULL, blank=True, null=True)

    partition = models.ForeignKey(to="netbox_e164.E164Partition", on_delete=models.SET_NULL, blank=True, null=True)

    trunk = models.ForeignKey(to="netbox_e164.E164Trunk", on_delete=models.SET_NULL, blank=True, null=True)

    deleted_on_ucm = models.BooleanField(default=False)

    deleted_on_ucm_date = models.DateTimeField(blank=True, null=True)

    objects = RestrictedQuerySet.as_manager()

    class Meta:
        """Provides default order for class"""

        ordering = ["deleted_on_ucm_date"]

    def __str__(self):
        """String representation of an E164 Number"""
        return f"{self.site} : {self.number}"


class E164Partition(PrimaryModel):
    """Sync'd with UCM for applying to E164Number model"""

    name = models.CharField(max_length=50)

    ucm = models.ForeignKey(to="dcim.Device", on_delete=models.PROTECT, blank=False, null=False)

    ucm_uuid = models.UUIDField(unique=True, blank=True, null=True)

    objects = RestrictedQuerySet.as_manager()

    csv_headers = [
        "name",
        "ucm",
    ]

    class Meta:
        """Provides default order for class"""

        ordering = ["name"]

    def __str__(self):
        """String representation of an E164 Partition"""
        return f"{self.name} : {self.ucm.name}"

    # def get_absolute_url(self):
    #     from django.urls import reverse
    #     return reverse('plugins:netbox_e164:e164partition_edit', args=[self.pk])


class E164Trunk(PrimaryModel):
    """Sync'd with UCM for applying to E164Number model"""

    name = models.CharField(max_length=50, blank=False, null=False)

    type = models.CharField(max_length=255, choices=E164TrunkTypes, blank=False, null=False)

    ucm = models.ForeignKey(to="dcim.Device", on_delete=models.PROTECT, blank=False, null=False)

    ucm_uuid = models.UUIDField(unique=True, blank=True, null=True)

    objects = RestrictedQuerySet.as_manager()

    csv_headers = [
        "name",
        "ucm",
    ]

    class Meta:
        """Provides default order for class"""

        ordering = ["name"]

    def __str__(self):
        """String representation of an E164 Number"""
        return f"{self.name} : {self.ucm.name}"


class E164Task(models.Model):
    """Table used to track synchronization requrests"""

    type = models.CharField(max_length=50, choices=E164TaskTypes)

    device = models.ForeignKey(to="dcim.Device", on_delete=models.PROTECT, blank=False, null=False)

    status = models.CharField(max_length=255, choices=E164TaskStatus, help_text="Overall status of the task")

    created_on = models.DateTimeField(auto_now_add=True)

    STATUS_CLASS_MAP = {
        E164TaskStatus.STATUS_FAILED: "danger",
        E164TaskStatus.STATUS_PENDING: "primary",
        E164TaskStatus.STATUS_RUNNING: "info",
        E164TaskStatus.STATUS_SUCCEEDED: "success",
    }

    def get_status_class(self):
        return self.STATUS_CLASS_MAP.get(self.status)

    class Meta:
        """Provides default order for class"""

        ordering = ["created_on"]

    def __str__(self):
        """String representation of an E164Task"""
        return f"{self.type} : {self.status}"


class E164TaskLogEntry(models.Model):
    """Table used to for event logging"""

    task = models.ForeignKey(to="netbox_e164.E164Task", on_delete=models.CASCADE, blank=False, null=False)

    source = models.CharField(max_length=255)

    message = models.TextField()

    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        """Provides default order for class"""

        ordering = ["created_on"]

    def __str__(self):
        """String representation of an E164TaskLogEntry"""
        return f"{self.source} : {self.message}"
