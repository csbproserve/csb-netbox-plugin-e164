"""Filtering logic for E164Number instances.

(c) 2020 C Spire Business
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import django_filters
from django.db.models import Q
from django.conf import settings

from dcim.models import Site, Device
from tenancy.models import Tenant
from netbox.filtersets import OrganizationalModelFilterSet, BaseFilterSet

from .models import E164Number, E164Partition, E164Trunk, E164Task, E164TaskLogEntry

PLUGIN_SETTINGS = settings.PLUGINS_CONFIG["netbox_e164"]


class E164NumberListFilter(OrganizationalModelFilterSet):
    """Filter capabilities for E164Number instances."""

    q = django_filters.CharFilter(method="search", label="Search",)

    site = django_filters.ModelMultipleChoiceFilter(
        field_name="site__slug", queryset=Site.objects.all(), to_field_name="slug", label="Site (slug)",
    )

    site_id = django_filters.ModelMultipleChoiceFilter(queryset=Site.objects.all(), label="Site (ID)",)

    tenant = django_filters.ModelMultipleChoiceFilter(
        field_name="tenant__slug", queryset=Tenant.objects.all(), to_field_name="slug", label="Tenant (slug)",
    )

    class Meta:  # noqa: D106 "Missing docstring in public nested class"
        model = E164Number
        fields = [
            "id",
            "number",
            "site",
            "site_id",
            "tenant",
        ]

    def search(self, queryset, name, value):
        """Perform the filtered search."""
        if not value.strip():
            return queryset
        qs_filter = (
            Q(id__icontains=value)
            | Q(number__icontains=value)
            | Q(site__name__icontains=value)
            | Q(tenant__name__icontains=value)
            | Q(description__icontains=value)
        )
        return queryset.filter(qs_filter)


class E164PartitionListFilter(BaseFilterSet):
    """Filter capabilities for E164Partition instances."""

    q = django_filters.CharFilter(method="search", label="Search",)

    ucm_role = PLUGIN_SETTINGS["ucm_role"]
    ucm = django_filters.ModelMultipleChoiceFilter(
        field_name="ucm__name", queryset=Device.objects.all(), to_field_name="name", label="Device (name)",
    )

    ucm_id = django_filters.ModelMultipleChoiceFilter(queryset=Device.objects.all(), label="Device (ID)",)

    class Meta:  # noqa: D106 "Missing docstring in public nested class"
        model = E164Partition
        fields = ["id", "name", "ucm", "ucm_id"]

    def search(self, queryset, name, value):
        """Perform the filtered search."""
        if not value.strip():
            return queryset
        qs_filter = Q(id__icontains=value) | Q(name__icontains=value) | Q(ucm__name__icontains=value)
        return queryset.filter(qs_filter)


class E164TrunkListFilter(BaseFilterSet):
    """Filter capabilities for E164Trunk instances."""

    q = django_filters.CharFilter(method="search", label="Search",)

    ucm_role = PLUGIN_SETTINGS["ucm_role"]
    ucm = django_filters.ModelMultipleChoiceFilter(
        field_name="ucm__name", queryset=Device.objects.all(), to_field_name="name", label="Device (name)",
    )

    ucm_id = django_filters.ModelMultipleChoiceFilter(queryset=Device.objects.all(), label="Device (ID)",)

    class Meta:  # noqa: D106 "Missing docstring in public nested class"
        model = E164Trunk
        fields = ["id", "name", "ucm", "ucm_id"]

    def search(self, queryset, name, value):
        """Perform the filtered search."""
        if not value.strip():
            return queryset
        qs_filter = Q(id__icontains=value) | Q(name__icontains=value) | Q(ucm__name__icontains=value)
        return queryset.filter(qs_filter)


class E164TaskListFilter(BaseFilterSet):
    """Filter capabilities for E164Task instances."""

    q = django_filters.CharFilter(method="search", label="Search",)

    class Meta:
        model = E164Task
        fields = ["id", "created_on", "device", "type", "status"]

    def search(self, queryset, name, value):
        """Perform the filtered search."""
        if not value.strip():
            return queryset
        qs_filter = (
            Q(id__icontains=value)
            | Q(created_on__icontains=value)
            | Q(device__name__icontains=value)
            | Q(type__name__icontains=value)
            | Q(status__name__icontains=value)
        )
        return queryset.filter(qs_filter)


class E164TaskLogEntryFilter(BaseFilterSet):
    """Filter capabilities for E164TaskLogEntry instances."""

    q = django_filters.CharFilter(method="search", label="Search",)

    class Meta:
        model = E164TaskLogEntry
        fields = ["id", "created_on", "task", "source", "message"]

    def search(self, queryset, name, value):
        """Perform the filtered search."""
        if not value.strip():
            return queryset
        qs_filter = (
            Q(id__icontains=value)
            | Q(created_on__icontains=value)
            | Q(task__type_icontains=value)
            | Q(source__icontains=value)
            | Q(message__icontains=value)
        )
        return queryset.filter(qs_filter)
