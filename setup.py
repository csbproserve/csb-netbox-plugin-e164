from setuptools import find_packages, setup

setup(
    name="csb-netbox-plugin-e164",
    version="0.1.0",
    description="A plugin to manage e164 numbers in Netbox",
    url="https://gitlab.com/csbproserve/csb-netbox-plugin-e164",
    author="Jeremy Sanders",
    author_email="csbdev@cspire.com",
    license="Apache 2.0",
    install_requires=["zeep", "napalm", "netmiko"],
    packages=find_packages(),
    include_package_data=True,
)
