"""NetBox configuration file overrides specific to version 2.11.12."""
from .base_configuration import *  # pylint: disable=relative-beyond-top-level, wildcard-import

# Overrides specific to this version go here
